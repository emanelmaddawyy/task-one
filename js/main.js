$(document).ready(function() {
  var brands = [
    {
      id: 1,
      name: "تسهيل"
    },
    {
      id: 2,
      name: "تقييم"
    },
    {
      id: 3,
      name: "توافق"
    },
    {
      id: 4,
      name: "توصيل"
    }
  ];
  
  let centers = [
    {
      id: 1,
      name: 'مركز محمود',
      brandId: 1
    },
    {
      id: 2,
      name: 'مركزايمان',
      brandId: 1
    },
    {
      id: 3,
      name: 'مركز اماني',
      brandId: 2
    },
    {
      id: 4,
      name: 'مركز سارة',
      brandId: 3
    }
  ]

  let options = {
    divBrands: '#div-center-types',
    divCenters: '#div-centers',
    btnBrand: '.btn-brand',
    data: {
      brandId: 'brandId'
    },
    selectedBrandId: null
  }
  
  
  //var i, len, text;
  // for (i = 0, len = brands.length, text = ""; i < len; i++) {
  //   text += brands[i] + "<br>";
  // }
  
  //var brandsDiv = document.getElementById('div-brands')
  
  // normal for loop
  // for (var i = 0; i < brands.length; i++) {
  //   // 1- get item from arr
  //   var item = brands[i];
  
  //   // 2- create btn html for item
  //   var button = '<button class="btn btn-brand">' + item.name + '</button>'
  
  //   // 3- add button html to document
  //   brandsDiv.innerHTML += button
  // }
  
  // foreach
  // function brandsCallnback(item, i) {
  //   console.log(item)
  //   console.log('i: ' + i)
  // }
  
  
  
  // var brandsDiv = document.getElementById('div-brands')
  
  // brands.forEach((brand, i) => {
  //   // 2- create btn html for brand
  //   var button = `<button class="btn btn-brand">${brand.name}</button>`
  
  //   // 3- add btn html to brands div (ui)
  //   brandsDiv.innerHTML += button
  // })
  
  
  
  
  
  
  
  
  
  
  
  
  // brands.forEach((item, i) => {
  //   console.log(item)
  //   console.log('i: ' + i)
  // })
  
  
  
  // function sayHello1(name) {
  //   console.log('Hello ' + name)
  // }
  
  // var sayHello2 = function(name) {
  //   console.log('Hello 2 ' + name)
  // }
  
  // var sayHello3 = (name) => {
  //   console.log('Hello 3 ' + name)
  // }
  
  // ((name) => {
  //   console.log('Hello 4 ' + name)
  // })('Eman')
  
  
  
  // Jquery version
  
  
  let brandsDiv = $(options.divBrands)
  
  brands.forEach((item, index) => {
    // create button html
    // ECMASCRIPT 6 
    let button = `<button class="btn btn-brand" data-brand-id="${item.id}">${item.name}</button>`
  
    // add to brands div
    brandsDiv.append(button)
  })
  
  
  $(options.btnBrand).on('click', function(event) {
    // get button
    let clickedButton = $(event.target)
  
    // get data-id from the clickedButton
    let clickedBrandId = clickedButton.data(options.data.brandId)
    options.selectedBrandId = clickedBrandId
    
  
    // get centers arr of clicked brand
    
    // filteraaaaaaation-------
    // centers.forEach((center, i) => {
    //   if (center.brandId == clickedBrandId) {
    //     filteredCenters.push(center)
    //   }
    // })
  
    let filteredCenters = centers.filter((center, i) => {
      return center.brandId == clickedBrandId
    })
  
    let centersDiv = $(options.divCenters)
    centersDiv.empty()
  
    filteredCenters.forEach((center, i) => {
      let button = `<button class="btn btn-warning btn-center" data-id="${center.id}">${center.name}</button>`
  
      // add to centers div
      centersDiv.append(button)
    })
  
  })
  
  
  
  
  // function add(n1, n2) {
  //   return n1 + n2
  // }
  
  
  
  // let z = add(1, 2) // 3
  
  
})